<?php
include('../connector/db-connector.php');
session_start();

if(!isset($_SESSION["attempt"])){
  header('Location:index.php');
}

function getAllReq(){

    $conn = getConnection();

    $stmt = $conn->prepare("SELECT * FROM contact_req ORDER BY req_updated_date DESC");
    $stmt->execute();
    $result = $stmt->get_result();

    if ($result->num_rows > 0) {
        return $result->fetch_all();
    } else {
        return null;
    }

    $stmt->close();
    $conn->close();
}

function getAllSubs(){

  $conn = getConnection();

  $stmt = $conn->prepare("SELECT * FROM subs ORDER BY subs_updated_date DESC");
  $stmt->execute();
  $result = $stmt->get_result();

  if ($result->num_rows > 0) {
      return $result->fetch_all();
  } else {
      return null;
  }

  $stmt->close();
  $conn->close();
}

?><!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/bootstrap.css">
    <link rel="stylesheet" href="../css/dashboard-main.css">
    <title>Xitricon | Dashboard</title>
</head>

<body>
    <nav class="nav-bar">
        <div class="row">
            <div class="col-md-6 secs">
                <img src="../assets/logo2.svg" alt="" srcset="">
            </div>
            <div class="col-md-6 secs">
                <button class="btn btn-primary" type="button" onclick="signOut()">Log out</button>
            </div>
        </div>

    </nav>
    <section>
        <div class="container">
            <ul class="nav nav-pills mb-3 justify-content-center" id="pills-tab" role="tablist">
                <li class="nav-item" role="presentation">
                    <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill"
                        data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home"
                        aria-selected="true">Contact Requests</button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link" id="pills-profile-tab" data-bs-toggle="pill"
                        data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile"
                        aria-selected="false">Subscribers</button>
                </li>
            </ul>
            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                    <table class="table table-striped">
                        <thead>
                          <tr>
                            <th scope="col">No.</th>
                            <th scope="col">Name</th>
                            <th scope="col">Company</th>
                            <th scope="col">Designation</th>
                            <th scope="col">Email</th>
                            <th scope="col">Contact</th>
                            <th scope="col">Submit Date</th>
                          </tr>
                        </thead>
                        <tbody>
                          <!-- <tr>
                            <th scope="row">1</th>
                            <td>Mark</td>
                            <td>makan pvt ltd</td>
                            <td>director</td>
                            <td><a href="mailto:mark@gmail.com">mark@gmail.com</a></td>
                            <td><a href="tel:0123456789">0712345678</a></td>
                            <td>2023-12-12</td>
                          </tr> -->
                          
                <?php
                
                $req = getAllReq();

                if($req != null) {
                  $reqCount = count($req);
                  for ($cou = 0; $cou < $reqCount; $cou++) {
                ?>
                          <tr>
                            <th scope="row"><?php echo $cou+1 ?></th>
                            <td><?php echo $req[$cou][1] ?></td>
                            <td><?php echo $req[$cou][2] ?></td>
                            <td><?php echo $req[$cou][3] ?></td>
                            <td><a href="mailto:<?php echo $req[$cou][4] ?>"><?php echo $req[$cou][4] ?></a></td>
                            <td><a href="tel:<?php echo $req[$cou][5] ?>"><?php echo $req[$cou][5] ?></a></td>
                            <td><?php echo $req[$cou][6] ?></td>
                          </tr>

                          <?php
                  }
                          }?>                          
                        </tbody>
                      </table>
                </div>
                <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <table class="table table-striped">
                        <thead>
                          <tr>
                            <th scope="col">No.</th>
                            <th scope="col">E-mail</th>
                            <th scope="col">Submit Date</th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php
                
                $sub = getAllSubs();

                if($sub != null) {
                  $subCount = count($sub);
                  for ($cou = 0; $cou < $subCount; $cou++) {
                ?>
                          <tr>
                            <th scope="row"><?php echo $cou+1 ?></th>
                            <td><a href="mailto:<?php echo $sub[$cou][1] ?>"><?php echo $sub[$cou][1] ?></a></td>
                            <td><?php echo $sub[$cou][2] ?></td>
                          </tr>
                          <?php
                  }
                          }?>    
                          
                        </tbody>
                      </table>
                </div>
            </div>
        </div>
    </section>

</body>
<script src="../js/bootstrap.js"></script>
<script>
  function signOut() {

var xhttp = new XMLHttpRequest();
xhttp.open("POST", "../api/main-api.php", true);
xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
xhttp.setRequestHeader("application-auth", "xitricon-auth");

xhttp.onreadystatechange = function () {
    if (xhttp.readyState === XMLHttpRequest.DONE && xhttp.status === 200) {
        if (xhttp.responseText) {
            var responseObj = JSON.parse(xhttp.responseText);
            if (responseObj) {
                if (responseObj.status === "SUCCESS") {
                    location.reload();
                } else {
                    alert(responseObj.msg);
                }
            }
        }
    }
}
xhttp.send("action=signout");
}
</script>
</html>