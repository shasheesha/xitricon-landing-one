<?php
session_start();

include('../connector/db-connector.php');

header('Content-Type: application/json');

function userLogin($username, $password){
    $sendOnj = [
        'status' => "ERROR",
        'msg' => "Authentication Failed. Try again."
    ];

    $conn = getConnection();


    if (isset($username) && isset($password)) {

        $stmt = $conn->prepare("SELECT * FROM user WHERE username = '".$username."'");
        // $stmt->bind_param("s", $username);
        $stmt->execute();
        $result = $stmt->get_result();

        if ($result->num_rows == 1) {
            $row = $result->fetch_assoc();
            $db_password = $row["password"];

            if ($password == $db_password) {
                $sendOnj = [
                    'status' => "SUCCESS",
                    'msg' => "Login OK"
                ];
                $_SESSION['attempt'] = 1;
            }else{
                $sendOnj = [
                    'status' => "UNSUCCESS",
                    'msg' => "Login Failed"
                ];
            }
        }
        $stmt->close();
        $conn->close();
    }

    return $sendOnj;
}

function userLogout(){
    session_destroy();
    if (isset($_SESSION['attempt'])) {
        $sendOnj = [
            'status' => "SUCCESS",
            'msg' => "logout OK"
        ];
    }else{
        $sendOnj = [
            'status' => "UNSUCCESS",
            'msg' => "logout Failed"
        ];
    }
    return $sendOnj;
}

function add_subscriber($email){

    $conn = getConnection();

    $sql = "INSERT INTO subs (subs_email)
    VALUES ('".$email."')";

if ($conn->query($sql) === TRUE) {
    $sendOnj = [
        'status' => "SUCCESS",
        'msg' => "Login OK"
    ];
   
    return $sendOnj;
} else {
    $sendOnj = [
        'status' => "UNSUCCESS",
        'msg' => "Login Failed"
    ];
    echo "Error: " . $sql . "<br>" . $conn->error;
    return $sendOnj;
}
}

function add_contactReq($name, $company, $iam, $contact, $email){

    $conn = getConnection();

    $sql = "INSERT INTO contact_req (req_name, req_company, req_designation, req_email, req_contact)
    VALUES ('".$name."', '".$company."', '".$iam."', '".$email."','".$contact."');";

    if ($conn->query($sql) === TRUE) {
        $sendOnj = [
            'status' => "SUCCESS",
            'msg' => "Login OK"
        ];
       
        return $sendOnj;
    } else {
        $sendOnj = [
            'status' => "UNSUCCESS",
            'msg' => "Login Failed"
        ];
        echo "Error: " . $sql . "<br>" . $conn->error;
        return $sendOnj;
    }
}

// Check if the request is an AJAX request
if (isset($_SERVER['HTTP_APPLICATION_AUTH']) && strtolower($_SERVER['HTTP_APPLICATION_AUTH']) === 'xitricon-auth') {


    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if (isset($_POST['action'])) {
            $action_param = $_POST['action'];
            if ($action_param === "signin") {
                echo json_encode(userLogin($_POST["username"], $_POST["password"]));
            } else if ($action_param === "signout") {
                echo json_encode(userLogout());
            } else if ($action_param === "add_subscriber") {
                echo json_encode(add_subscriber($_POST["email"]));
            } else if ($action_param === "add_contactreq") {
                echo json_encode(add_contactReq($_POST["name"],$_POST["company"],$_POST["iam"],$_POST["contact"],$_POST["email"]));
            }
        }
    }
}
?>