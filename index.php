<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./css/bootstrap.css">
    <link rel="stylesheet" href="./css/main.css">
    <title>Xitricon</title>
</head>

<body>
    <!-- <nav class="nav-bar">
        <div class="row">
            <div class="secs col-6">
                <img src="./assets/logo2.svg" alt="" srcset="">
            </div>
            <div class="secs col-6">
                <img src="./assets/IFS_Icon_Gold-Services-Partner_Positive.png" alt="" srcset="">
            </div>
        </div>
    </nav> -->
    <section class="hero-banner">
    
    <div class="overlay"></div>    
    <video
        loop 
        muted 
        autoplay 
        preload="auto">
        <source src="./assets/videos/Sequence 01_4.mp4" type="video/mp4">
        your browser does not support the video tag.
    </video>
        <nav class="nav-bar">
            <div class="row">
                <div class="secs col-6">
                    <img id="main-logo" class="main-logo" src="./assets/logo1.svg" alt="" srcset="">
                </div>
                <div class="secs secs-2 col-6">
                <div class="logo-area">

<img src="./assets/IFS-white.svg" alt="" srcset="">
<p class="ifs-whtie-para">
    <strong>IFS CUSTOMER<br>CHOICE PARTNER</strong><br>OF THE YEAR 2022
</p>
</div>
                    <!-- <img id="ifs-gold-service" class="ifs-gold-service" src="./assets/IFS_Icon_Gold-Services-Partner_Positive.png" alt=""
                        srcset=""> -->
                </div>
            </div>
        </nav>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1 class="hero-title">Enterprise Asset Management<br>(EAM) Software Solution</h3>
                    <h4 class="sub-title-two">Maximize assets value, improve safety and boost productivity</h4>
                    <h3 class="sub-title">Elevate Your Business with Xitricon and IFS:<br>
Empowering Enterprises with Advanced EAM Software and<br>Implementation Services.</h1>
                    <h2 class="min-title">• Contact Us •</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <form action="" id="form-1" class="needs-validation" novalidate>
                        <div class="input-fields">
                            <div class="form-floating">
                                <input type="text" class="form-control" id="name" placeholder="ex: Jhone" required>
                                <label for="name">Name</label>
                                <!-- <div class="valid-feedback">Looks good!</div> -->
                                <div class="invalid-feedback">Please provide your name.</div>
                            </div>
                            <div class="form-floating">
                                <input type="text" class="form-control" id="company"
                                    placeholder="ex: Xitricon (PVT) LTD" required>
                                <label for="company">Company</label>
                                <!-- <div class="valid-feedback">Looks good!</div> -->
                                <div class="invalid-feedback">Please provide your company.</div>
                            </div>
                            <div class="form-floating">
                                <select class="form-select form-control" id="iam" aria-label="i-am-select" required>
                                    <option selected>I am</option>
                                    <option value="Director">Director</option>
                                    <option value="Executive">Executive</option>
                                    <option value="Employee">Employee</option>
                                </select>
                                <!-- <label for="floatingSelect">I am</label> -->

                                <!-- <div class="valid-feedback">Looks good!</div> -->
                                <div class="invalid-feedback">Please provide your designation.</div>
                            </div>
                            <div class="form-floating">
                                <input type="number" class="form-control" id="contact" placeholder="ex: 012 345 6789"
                                    required>
                                <label for="contact">Contact Number</label>
                                <!-- <div class="valid-feedback">Looks good!</div> -->
                                <div class="invalid-feedback">Please provide your contact Number.</div>
                            </div>
                            <div class="form-floating">
                                <input type="email" class="form-control" id="email" placeholder="ex: name@example.com"
                                    required>
                                <label for="email">Email</label>
                                <!-- <div class="valid-feedback">Looks good!</div> -->
                                <div class="invalid-feedback">Please provide your e-mail address.</div>
                            </div>
                        </div>
                        <p class="orange-text">Please provide the required information, and one of our agents will  contact you.</p>
                        <button class="btn btn-primary cus-primary" onclick="contactReq()" type="button">send</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <section class="discover-sec">
        <div class="container">
            <div class="row">
                <div class="col-12 side-sec">
                    <h2 class="secondary-title">Discover the Benefits of Elevating<br>Your Productivity with EAM Solutions
                    </h2>
                    <p class="primary-para" style="padding-right: 100px;">Gain a comprehensive view of your asset
                        position and enhance asset availability, reliability, and the services you provide with IFS EAM
                        software. Monitor asset health, drive predictive maintenance, and swiftly resolve failures
                        without impacting productivity.
                    </p>
                </div>
                <!-- <div class="col-md-5 side-sec side-img-sec-1">
                    <img src="./assets/Goes2.png" alt="">
                </div> -->
            </div>
        </div>
    </section>
    <section class="video-sec">
        <div class="container">
            <div class="row">
                <div class="col-md-7 side-sec side-img-sec-2">
                    <img style="cursor: pointer;" class="video-play" onclick="videoModal();" src="./assets/video-play.png" alt="" srcset="">
                </div>
                <div class="col-md-5 side-sec">
                    <h2 class="secondary-title">
                        Learn more about IFS EAM Software.
                    </h2>
                    <p style="cursor: pointer;" onclick="videoModal();" class="hyper-para">View Video</p>
                </div>
            </div>
        </div>
    </section>
    <section class="brochure">
        <div class="container">
            <div class="row ">
                <div class="col-md-6 col-side side-sec-two">
                    <div class="brochure-card">
                        <h3 class="card-title">Reasons to Choose IFS<br>Enterprise Asset Management</h3>
                        <img class="brochure-img" src="./assets/Rectangle 6.png" alt="" srcset="">
                        <a class="hyper-para" target="_blank" href="http://enrol-technology.000webhostapp.com/sync/xitricon/assets/pdf/IFS_Infographic_10-Reasons-IFS-EAM_04_2022.pdf"><img class="icon" src="./assets/icons/arrow.svg" alt="">View
                            Brochure</a>
                    </div>
                </div>
                <div class="col-md-6 col-side side-sec-two">
                    <div class="brochure-card">
                        <h3 class="card-title">Build a resilient enterprise with<br>EAM technology</h3>
                        <img class="brochure-img" src="./assets/Rectangle 7.png" alt="" srcset="">
                        <a class="hyper-para" target="_blank" href="http://enrol-technology.000webhostapp.com/sync/xitricon/assets/pdf/IFS_Bro_Cloud_EAM_02_2023.pdf"><img class="icon" src="./assets/icons/arrow.svg" alt="">View
                            Brochure</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="dark-sec">
        <div class="container">
            <!-- <div class="row ifs-logo-area">
                <div class="col-12 fw-row">
                    <div class="logo-area">

                        <img src="./assets/IFS-white.svg" alt="" srcset="">
                        <p class="ifs-whtie-para">
                            <strong>IFS CUSTOMER CHOICE PARTNER</strong><br>OF THE YEAR 2022
                        </p>
                    </div>
                </div>
            </div> -->
            <div class="row">
                <div class="col-12">
                    <h2 class="secondary-title third-color">Discover Our Services</h2>
                    <p class="primary-para white-para">Xitricon is a leading consultancy, technology, and digital
                        transformation services company that offers a comprehensive suite of solutions to help
                        organizations successfully navigate their digital transformation journey. With extensive
                        industry expertise, global reach, and advanced solutions, Xitricon empowers enterprises to
                        maintain a competitive edge. As an IFS Gold Services Partner, Xitricon further enhances its
                        capabilities and offerings to provide exceptional services and support in the implementation and
                        optimization of IFS solutions.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <h2 class="secondary-title third-color">Why Choose Xitricon?</h2>
                    <div class="row">
                        <div class="col-md-6">
                            <ul class="point-ul ul-para white-para">
                                <li>Proven track record of successfully implementing/upgrading ERP solutions.</li>
                                <li>Deep expertise and knowledge of IFS ERP, including its features, modules, and
                                    integration capabilities.</li>
                                <li>Customisation and configuration expertise.</li>
                                <li>Well-defined IFS Project Management and Implementation Methodology for smooth
                                    project execution.</li>
                                <li>Technical expertise and integration capabilities.</li>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <ul class="point-ul ul-para white-para">
                                <li>Business Value Assessment pre-study on the current version.</li>
                                <li>Assistance with solution mapping.</li>
                                <li>Best practices followed by our experts.</li>
                                <li>Expertise in working with Developer Desktop/Cloud Build Place.</li>
                                <li>Solution verification by our expert consultants.</li>
                                <li>Post-go-live support.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row icon-sec">
                <div class="col-12">
                    <h2 class="secondary-title third-color">What We Offer</h2>
                    <div class="row icon-area">
                        <div class="col-md-6 col-xl-3 icon-card">
                            <img src="./assets/icons/1 (1).svg" alt="">
                            <h4 class="icon-para white-para">Implementation</h4>
                        </div>
                        <div class="col-md-6 col-xl-3 icon-card">
                            <img src="./assets/icons/1 (5).svg" alt="">
                            <h4 class="icon-para white-para">Software Licensing</h4>
                            <p class="fade-para">IFS, Boomi, Infor</p>
                        </div>
                        <div class="col-md-6 col-xl-3 icon-card">
                            <img src="./assets/icons/1 (7).svg" alt="">
                            <h4 class="icon-para white-para">Technical Services</h4>
                        </div>
                        <div class="col-md-6 col-xl-3 icon-card">
                            <img src="./assets/icons/1 (8).svg" alt="">
                            <h4 class="icon-para white-para">Ugrades &<br>Customizations</h4>
                        </div>
                        <div class="col-md-6 col-xl-3 icon-card">
                            <img src="./assets/icons/1 (6).svg" alt="">
                            <h4 class="icon-para white-para">AMS & Support<br>Services</h4>
                        </div>
                        <div class="col-md-6 col-xl-3 icon-card">
                            <img src="./assets/icons/1 (4).svg" alt="">
                            <h4 class="icon-para white-para">Resource<br>Augmentation</h4>
                        </div>
                        <div class="col-md-6 col-xl-3 icon-card">
                            <img src="./assets/icons/1 (3).svg" alt="">
                            <h4 class="icon-para white-para">Training</h4>
                        </div>
                        <div class="col-md-6 col-xl-3 icon-card">
                            <img src="./assets/icons/1 (2).svg" alt="">
                            <h4 class="icon-para white-para">Navulia<br>E-Procurement</h4>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <footer>
        <div class="container">
            <div class="row footer-top">
                <div class="col-md-4 footer-logo">
                    <img class="main-logo" src="./assets/logo2.svg" alt="" srcset="">
                </div>
                <div class="col-md-8 nav-menu">
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">Services</a></li>
                        <li><a href="#">Navulia E-Procurement</a></li>
                        <li><a href="#">Contact Us</a></li>
                    </ul>
                </div>
            </div>
            <div class="row top-border footer-bottom">
                <div class="col-md-6 other-links-area">
                    <a href="#">Privacy Policy</a>
                    <a href="#">Terms & Conditions</a>
                    <a href="#">GDPR Compliance</a>
                </div>
                <div class="col-md-6 social-media">
                    <p class="hint-text">Follow us on</p>
                    <div class="media-icon-area">
                        <div class="media-icon">
                            <a href="https://www.linkedin.com/company/xitricon-private-limited/">
                                <img src="./assets/icons/Layer 1 8.svg" alt="">
                            </a>    
                        </div>
                        <div class="media-icon">
                            <a href="https://www.facebook.com/xitricon">
                                <img src="./assets/icons/Layer 2 5.svg" alt="">
                            </a>
                        </div>
                        <div class="media-icon">
                            <a href="https://twitter.com/xitricon">
                                <img src="./assets/icons/Layer 3 2.svg" alt="">
                            </a>
                        </div>
                        <div class="media-icon">
                            <a href="https://www.instagram.com/xitricon/">
                                <img src="./assets/icons/Layer 4 1.svg" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
  
  <!-- Modal -->
    <div class="modal fade subscribe-modal video-modal" id="video-modal" tabindex="-1" aria-labelledby="video-modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content" style="padding:0px !important;">
        <!-- <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div> -->
        <div class="modal-body">
            <video width="100%" height="auto" controls>
                <source src="https://enrol-technology.000webhostapp.com/sync/xitricon/assets/videos/EAM_ALM_Video.mp4" type="video/mp4">
                <!-- <source src="movie.ogg" type="video/ogg"> -->
                Your browser does not support the video tag.
            </video>
        </div>
      </div>
    </div>
  </div>
  
  <div class="modal fade subscribe-modal" id="subscribe-modal" tabindex="-1" aria-labelledby="subscribe-modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <!-- <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div> -->
        <div class="modal-body">
            <h3 class="title-text">Join the Newsletter</h3>
          
            <p class="primary-para">Stay in the loop! Enter your email below to receive the latest updates on EAM.</p>
          <div class="subscribe-form-area">
            <form action="" method="">
                <div class="form-floating">
                    <input type="email" class="form-control" id="newsletter_email" placeholder="ex: name@example.com"
                        required>
                    <label for="email">Email</label>
                    <!-- <div class="valid-feedback">Looks good!</div> -->
                    <div class="invalid-feedback">Please provide e-mail address.</div>
                </div>
                <button class="btn btn-primary cus-primary" onclick="addNewSubscriber()" type="button">Subscribe</button>
            </form>
          </div>
            <button type="button" class="btn btn-secondary trans-close-btn" data-bs-dismiss="modal">Close</button>
          
        </div>
        <!-- <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div> -->
      </div>
    </div>
  </div>
</body>
<script
  src="https://code.jquery.com/jquery-3.7.0.js"
  integrity="sha256-JlqSTELeR4TLqP0OG9dxM7yDPqX1ox/HfgiSLBj8+kM="
  crossorigin="anonymous"></script>
<script src="./js/bootstrap.js"></script>
<script src="./js/main.js"></script>
<script>
    $(window).on('load', function() {
        $('#subscribe-modal').modal('show');
    });
    
    function videoModal(){
        $('#video-modal').modal('show');
    }
    
    function addNewSubscriber() {
    let email = document.getElementById('newsletter_email');
    const xhr = new XMLHttpRequest();
    xhr.open('POST', 'api/main-api.php', true);
    xhr.setRequestHeader("application-auth", "xitricon-auth");

    const formData = new FormData();
    formData.append('email', email.value);
    formData.append('action', "add_subscriber");

    xhr.onload = function () {
        if (xhr.readyState === xhr.DONE && xhr.status === 200) {
            if (xhr.responseText) {
                var responseObj = JSON.parse(xhr.responseText);
                if (responseObj) {
                    if (responseObj.status === "SUCCESS") {
                        // location.reload();
                        $('#subscribe-modal').modal('toggle');
                    } else {
                    alert(responseObj.msg);
                    }
                }
            }
        }
    };
    xhr.send(formData);
}



function contactReq() {
    let name = document.getElementById("name");
    let company = document.getElementById("company");
    let iam = document.getElementById("iam");
    let contact = document.getElementById("contact");
    let email = document.getElementById("email");

    const xhr = new XMLHttpRequest();
    xhr.open('POST', 'api/main-api.php', true);
    xhr.setRequestHeader("application-auth", "xitricon-auth");

    const formData = new FormData();
    formData.append('name', name.value);
    formData.append('company', company.value);
    formData.append('iam', iam.value);
    formData.append('contact', contact.value);
    formData.append('email', email.value);
    formData.append('action', "add_contactreq");

    xhr.onload = function () {
        if (xhr.readyState === xhr.DONE && xhr.status === 200) {
            if (xhr.responseText) {
                var responseObj = JSON.parse(xhr.responseText);
                if (responseObj) {
                    if (responseObj.status === "SUCCESS") {
                        // location.reload();
                        $('#form-1')[0].reset();
                    } else {
                    alert(responseObj.msg);
                    }
                }
            }
        }
    };
    xhr.send(formData);
}
    
</script>
</html>